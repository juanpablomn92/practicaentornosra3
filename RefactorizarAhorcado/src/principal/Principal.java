package principal;

import java.io.File;
import java.util.Scanner;

public class Principal {
	private final static byte NUM_PALABRAS = 20;
	private final static byte FALLOS = 7;
	private static String[] palabras = new String[NUM_PALABRAS];

	public static void main(String[] args) {

		String palabraSecreta="";
		String caracteresElegidos = "";
		int fallos=0;
		boolean acertado=false;
		lecturaFichero();
		Scanner input = new Scanner(System.in);

		palabraSecreta = palabraSecreta();

		char[][] caracteresPalabra = new char[2][];
		caracteresPalabra[0] = palabraSecreta.toCharArray();
		caracteresPalabra[1] = new char[caracteresPalabra[0].length];

		
		System.out.println("Acierta la palabra");
		do {

			System.out.println("####################################");

			pintaPalabra(caracteresPalabra);
			System.out.println();

			caracteresElegidos = peticionInformacion(caracteresElegidos, input);
			fallos = 0;

			boolean encontrado;
			fallos = compruebaPalabra(caracteresElegidos, fallos, caracteresPalabra);

			dibujoFallos(fallos);

			acertado = ganadoOPerdido(palabraSecreta, fallos, caracteresPalabra);

		} while (!acertado && fallos < FALLOS);

		input.close();
	}

	private static int compruebaPalabra(String caracteresElegidos, int fallos, char[][] caracteresPalabra) {
		boolean encontrado;
		for (int j = 0; j < caracteresElegidos.length(); j++) {
			encontrado = false;
			for (int i = 0; i < caracteresPalabra[0].length; i++) {
				if (caracteresPalabra[0][i] == caracteresElegidos.charAt(j)) {
					caracteresPalabra[1][i] = '1';
					encontrado = true;
				}
			}
			if (!encontrado)
				fallos++;
		}
		return fallos;
	}

	private static boolean ganadoOPerdido(String palabraSecreta, int fallos, char[][] caracteresPalabra) {
		boolean acertado;
		if (fallos >= FALLOS) {
			System.out.println("Has perdido: " + palabraSecreta);
		}
		acertado = true;
		acertado = compruebaAcierto(acertado, caracteresPalabra);
		if (acertado)
			System.out.println("Has Acertado ");
		return acertado;
	}

	private static boolean compruebaAcierto(boolean acertado, char[][] caracteresPalabra) {
		for (int i = 0; i < caracteresPalabra[1].length; i++) {
			if (caracteresPalabra[1][i] != '1') {
				acertado = false;
				break;
			}
		}
		return acertado;
	}

	private static String peticionInformacion(String caracteresElegidos, Scanner input) {
		System.out.println("Introduce una letra o acierta la palabra");
		System.out.println("Caracteres Elegidos: " + caracteresElegidos);
		caracteresElegidos += input.nextLine().toUpperCase();
		return caracteresElegidos;
	}

	private static void pintaPalabra(char[][] caracteresPalabra) {
		for (int i = 0; i < caracteresPalabra[0].length; i++) {
			if (caracteresPalabra[1][i] != '1') {
				System.out.print(" -");
			} else {
				System.out.print(" " + caracteresPalabra[0][i]);
			}
		}
	}

	private static String palabraSecreta() {
		String palabraSecreta;
		palabraSecreta = palabras[(int) (Math.random() * NUM_PALABRAS)];
		return palabraSecreta;
	}

	private static void lecturaFichero() {
		String palabraSecreta, ruta = "src\\palabras.txt";
		File fich = new File(ruta);
		Scanner inputFichero = null;

		try {
			inputFichero = new Scanner(fich);
			for (int i = 0; i < NUM_PALABRAS; i++) {
				palabras[i] = inputFichero.nextLine();
			}
		} catch (Exception e) {
			System.out.println("Error al abrir fichero: " + e.getMessage());
		} finally {
			if (fich != null && inputFichero != null)
				inputFichero.close();
		}
	}

	private static void dibujoFallos(int fallos) {
		switch (fallos) {
		case 1:

			System.out.println("     ___");
			break;
		case 2:

			System.out.println("      |");
			System.out.println("      |");
			System.out.println("      |");
			System.out.println("     ___");
			break;
		case 3:
			System.out.println("  ____ ");
			System.out.println("      |");
			System.out.println("      |");
			System.out.println("      |");
			System.out.println("     ___");
			break;
		case 4:
			System.out.println("  ____ ");
			System.out.println(" |    |");
			System.out.println("      |");
			System.out.println("      |");
			System.out.println("     ___");
			break;
		case 5:
			System.out.println("  ____ ");
			System.out.println(" |    |");
			System.out.println(" O    |");
			System.out.println("      |");
			System.out.println("     ___");
			break;
		case 6:
			System.out.println("  ____ ");
			System.out.println(" |    |");
			System.out.println(" O    |");
			System.out.println(" T    |");
			System.out.println("     ___");
			break;
		case 7:
			System.out.println("  ____ ");
			System.out.println(" |    |");
			System.out.println(" O    |");
			System.out.println(" T    |");
			System.out.println(" A   ___");
			break;
		}
	}

}
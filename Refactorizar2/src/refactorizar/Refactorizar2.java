package refactorizar;

import java.util.Scanner;

public class Refactorizar2 {
	static Scanner teclado;
	public static void main(String[] args) {
		// TODO Auto-generated method stub
		/*
		 * Hay que dar a las variables un nombre identificativo 
		 * La cantidad de alumno es una costante 
		 * El tama�o del vector alumno es una constante
		 * Hay que declarar una sola variable por l�nea
		*/
		teclado = new Scanner(System.in);
		
		final int cantidad_maxima_alumnos =10;
			
		int[] alumnos= new int[10];
		peticionNotaMedia(teclado, cantidad_maxima_alumnos, alumnos);	
		
		System.out.println("El resultado es: " + muestraAlumnos(alumnos));
		
		teclado.close();
	}
	private static void peticionNotaMedia(Scanner teclado, final int cantidad_maxima_alumnos, int[] alumnos) {
		for(int n=0; n<cantidad_maxima_alumnos;n++)
		{
			System.out.println("Introduce nota media de alumno");
			alumnos[n] = teclado.nextInt();
		}
	}
	static double muestraAlumnos(int []alumnos)
	{
		double sumaNotas = 0;
		for(int a = 0 ; a < 10 ; a++) 
		{
			sumaNotas += alumnos[a];
		}
		return (sumaNotas/10);
	}
}
